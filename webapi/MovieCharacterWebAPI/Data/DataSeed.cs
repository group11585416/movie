﻿using MovieCharacterWebAPI.Model;
using System.Collections.Generic;

namespace MovieCharacterWebAPI.Data
{
    public class DataSeed
    {

        public static List<Movie> GetMovies()
        {
            List<Movie> movies = new List<Movie>();

            {
                new Movie()
                {
                    Id_Movie = 1,
                    MovieTitle = "The Fellowship of the Ring",
                    Genre = "Fantasy",
                    ReleaseYear = new System.DateTime(2001,12,1),
                    Director = "Peter Jackson",
                    picture = "https://en.wikipedia.org/wiki/The_Lord_of_the_Rings:_The_Fellowship_of_the_Ring#/media/File:The_Lord_of_the_Rings,_TFOTR_(2001).jpg",
                    Trailer = "https://www.youtube.com/watch?v=V75dMMIW2B4",
                   FranchiseId = 1
                };
                new Movie()
                {
                    Id_Movie = 2,
                    MovieTitle = "The Dark knight",
                    Genre = "Action",
                    ReleaseYear = new System.DateTime(2008,06,14),
                    Director = "Christoper Nolan",
                    picture = "https://en.wikipedia.org/wiki/The_Dark_Knight#/media/File:The_Dark_Knight_(2008_film).jpg",
                    Trailer = "https://www.youtube.com/watch?v=EXeTwQWrcwY",
                   FranchiseId = 2,
                   
                    
                };
                new Movie()
                {
                    Id_Movie = 3,
                    MovieTitle = "Spider-Man2",
                    Genre = "Action",
                    ReleaseYear = new System.DateTime(2004,06,30),
                    Director = "Sam Rami",
                    picture = "https://en.wikipedia.org/wiki/Spider-Man_2#/media/File:Spider-Man_2_Poster.jpg",
                    Trailer = "https://www.youtube.com/watch?v=EXeTwQWrcwY",
                   FranchiseId = 3
                };

            }

            return movies;

        }
        public static List<Franchise> GetFranchise()
        {
            List<Franchise> franchises = new List<Franchise>() {

            new Franchise()
            {
               Id_Franchise=1,
                name="The Lord of the Rings",
                description="Fantasy movie about a ring"

            },
             new Franchise()
            {
             Id_Franchise=2,
                name="DC cinematic",
                description="Action movie about a superhero dressed as a bat"

            },
               new Franchise()
            {
               Id_Franchise=3,
                name="Marvel",
                description="Action movie about a superhero that can shoot spiderweb"

            }

            };
            return franchises;
        }
        public static List<Character> getCharacters()
        {
            List<Character> characters = new List<Character>() {

            new Character()
            {
             Id_Character=1,
               FullName= "Viggo Mortensen",
               alias="Aragorn",
               Gender="Male",
               picture="https://no.wikipedia.org/wiki/Viggo_Mortensen#/media/Fil:Viggo_Mortensen_B_(2020).jpg"

            },
             new Character()
            {
             Id_Character=2,
               FullName= "Christian Bale",
               alias="Bruce Wayne",
               Gender="Male",
               picture="https://en.wikipedia.org/wiki/Christian_Bale#/media/File:Christian_Bale-7837.jpg"

            },
               new Character()
            {
              Id_Character=3,
               FullName= "Toby Maguire",
               alias="Spider-Man",
               Gender="Male",
               picture="https://en.wikipedia.org/wiki/Spider-Man_2#/media/File:Spider-Man_2_Poster.jpg"

            }

            };
            return characters;
        }
    }
}
