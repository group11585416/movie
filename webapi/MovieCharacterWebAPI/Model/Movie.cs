﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharacterWebAPI.Model
{
    public class Movie
    {
        [Key]
        public int Id_Movie { get; set; }


        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public DateTime ReleaseYear { get; set; }
      
        public string Director { get; set; }

        public string picture { get; set; }
        public string Trailer { get; set; }

        //Navigation

        public int FranchiseId { get; set; }
        public Franchise Franchise { get; set; }

        public ICollection<Character> CharacterMovie { get; set; }


        
        
        
        
    
        

    }
}
