﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterWebAPI.Data;
using System.Security.Policy;

namespace MovieCharacterWebAPI.Model
{
    public class MoviesDbContext : DbContext
    {
        public MoviesDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters{ get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
          modelBuilder.Entity<Movie>().HasData(DataSeed.GetMovies());
          modelBuilder.Entity<Franchise>().HasData(DataSeed.GetFranchise());
          modelBuilder.Entity<Character>().HasData(DataSeed.getCharacters());

        }
        
    }
}
