﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterWebAPI.Model
{
    public class Character
    {

        [Key]
        public int Id_Character { get; set; }
        public string FullName { get; set; }

        public string alias { get; set; }

        public string Gender { get; set; }
        public string picture { get; set; }



        //navigation
        public ICollection<Movie> CharacterMovie{ get; set; }
        


    }
}
