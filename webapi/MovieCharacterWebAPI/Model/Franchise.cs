﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterWebAPI.Model
{
    public class Franchise
    {
        [Key]
        public int Id_Franchise { get; set; }
        public string name { get; set; }
        public string description { get; set; }


        //navigation
        public ICollection<Movie> Movies { get; set; }
    }
}
